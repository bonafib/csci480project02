# main.py for raytracer2014
# Geoffrey Matthews

import os, pygame
from randomSpheres import *
from pygame.locals import *
from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *

if __name__ == "__main__":
    main_dir = os.getcwd()
else:
    main_dir = os.path.split(os.path.abspath(__file__))[0]
data_dir = os.path.join(main_dir, 'data')

def handleInput(screen):
    for event in pygame.event.get():
        if event.type == QUIT:
            return True
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                return True
            elif event.key == K_s:
                pygame.event.set_blocked(KEYDOWN|KEYUP)
                fname = raw_input("File name? ")
                pygame.event.set_blocked(0)
                pygame.image.save(screen,fname)
    return False

def main():
    pygame.init()
    screen = pygame.display.set_mode((512,512))
    pygame.display.set_caption('Raytracing!')
 
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((1,0,0))
 
    screen.blit(background, (0, 0))
    pygame.display.flip()

    going = True
    pixelsize = 128 # power of 2
    width, height = screen.get_size()
    world = World([Sphere((8,8,-16),5,Reflector(color=(1,1,1))),
                   Sphere((-12,4,-16),5,Reflector(color=(1,1,1))),
                   Sphere((-8,-3,-16),5,Phong(color=(0,0,1))),
                   Plane(point=( 0,15,-30),material=Image ("swirl_pattern.png", 4)),
                   randomPlaneBlob(),
#                   Sphere((8,8,-35),5,Phong(color=(0,1,0))),
#                   Sphere((-8,8,-20),5,Phong(color=(0,0,1))),
#                    Sphere((-8,-3,-17),5,Phong(color=(.7,.2,.4))),
#                   Sphere((8,14,-11),5,Phong(color=(.8,.9,.10))),
#                   Sphere((0,8,-22),5,Phong(color=(.8,.7,.3))),
                   Plane(material=Image("floor.png",8))],
                   [Light()],
                   Camera())  
    #world = ThreeSpheres()
    #world = RandomSpheres(n=32)
    #world = World()
    while going:
        going = not(handleInput(screen))
        while pixelsize > 0:
            for x in range(0,width,pixelsize):
                xx = x/float(width)
                for y in range(0,height,pixelsize):
                    #clock.tick(2)
                    yy = y/float(height)
                    # draw into background surface
                    #print("x = " + str(xx) + " y = " + str(yy))
                    color = world.colorAt(xx,yy)
                    color = [int(255*c) for c in color]
                    r,g,b = color
                    color = pygame.Color(r,g,b,255)#.correct_gamma(1/2.2)
                    background.fill(color, ((x,y),(pixelsize,pixelsize)))
                    if handleInput(screen):
                        return
        #draw background into screen
            screen.blit(background, (0,0))
            print("I made it")
            pygame.display.flip()

            print(pixelsize)
            pixelsize /= 2

#if this file is executed, not imported
if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()