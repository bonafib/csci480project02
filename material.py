import numpy
import os
import pygame
from vector import *
import ray as ry

def castShadow(point, world,light):
    lRay = ry.Ray(point, light.direction)
    for obj in world.objects:
        temp = obj.hit(lRay)
        if obj.castShadows() and temp != None and temp[0] > .001:
            return True
    return False


class Material():
    def colorAt(self, point, normal, ray, world, count=0):
        return vec(1,1,1)

class Flat(Material):
    def __init__(self, color = (0.25, 0.5, 1.0)):
        self.color = vec(color)
    def colorAt(self,point,normal, ray, world, count=0):      
        for light in world.lights:        
            if(castShadow(point,world, light)):
                return 0.30 *self.color
        return self.color

class Phong(Material):
    def __init__(self,
                 color=(0.25, 0.5, 1.0),
                 specularColor=(1,1,1),
                 ambient = 0.2,
                 diffuse = 1.0,
                 specular = 0.5,
                 shiny = 64,
                 shadows = False):
        self.color = vec(color[0],color[1],color[2])
        self.specularColor = vec(specularColor[0],specularColor[1],specularColor[2])
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny
        self.shadows = shadows
                       
    def colorAt(self,point,normal, ray, world, count=0):
        ambient = vec(1,1,1)
        viewer = normalize(ray.point - point)
        light = vec(0,0,0)
        light = (self.color*self.ambient)# * (ambient*self.ambient)
        for source in world.lights:
            if(not castShadow(point, world, source)):
                diffuse = self.color*self.diffuse*(max(0.0,numpy.dot(source.direction,normal)))
                light += diffuse
                spec = self.specularColor*self.specular*(max(numpy.dot(reflect(source.direction,normal),viewer),0))**self.shiny
                light += spec
        return vec(min(1,light[0]), min(1,light[1]), min(1,light[2]))
                    
class Reflector(Phong):
    def colorAt(self, point, normal, ray, world, count=0):
        count += 1
        if count > 5:
            return self.color
#        print("ray = " + str(ray.vector))
#        print("normal = " + str(normal))
#        print("reflect = " + str(reflect(ray.vector,normal)))
        ray = ry.Ray(point,reflect(-ray.vector,normal))
        hit = ray.closestReflect(world)
        if not hit  == None:
            return hit[3].material.colorAt(hit[1], hit[2],ray,world,count)*.85
        return world.neutral
        
class Image(Flat):
    def __init__(self, name, imageScale, color=(0.25, 0.5, 1.0)):
        self.color = vec(color)
        self.imageScale = imageScale
#        fullname = os.path.join('data', name)
        self.image = pygame.image.load(name)
        try:
            image = pygame.image.load(name)
        except pygame.error, message:
            print 'Cannot load image:', name
            raise SystemExit, message
        self.image = self.image.convert()
        print(image.get_rect())        
        print(self.image.get_width())
        print(self.image.get_height())
#         
    def colorAt(self, point, normal, ray, world, count=0):
        x = point[0]*self.imageScale
        u = int(round(x)) % self.image.get_width()
        z = point[2]*self.imageScale
        v = int(round(z)) % self.image.get_height()
        color = self.image.get_at([u,v])
        r = float(color[0])/255
        g = float(color[1])/255
        b = float(color[2])/255
        colorFloat = vec(r,g,b)
        for source in world.lights:
            if(castShadow(point,world, source )):
                colorFloat = colorFloat*.30   
        return colorFloat
#        