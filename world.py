import random
from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *
from vector import *


class AbstractWorld():
    def colorAt(self, x, y):
        return (random.random(),random.random(),random.random())

class World(AbstractWorld):
    def __init__(self,
                 objects=None,
                 lights=None,
                 camera=None,
                 maxDepth = 0,
                 neutral = (.5,.5,.5),
                 nsamples = 1,
                 gamma = 1):
                     self.objects = objects
                     self.lights = lights
                     self.camera = Camera()
                     self.maxDepth = maxDepth
                     self.neutral = vec(neutral[0],neutral[1],neutral[2])
                     self.nsamples = nsamples
                     self.gamma = gamma  
    
    def colorAt(self,x,y):
        vctr = self.camera.ray(x,y)
        #print("point = " + str(vctr.point) + "  x = " + str(x)+ "  y = " + str(y))
       # print("x = " + str(x) + " y = " + str(y))
        hit = Ray.closestHit(vctr,self)
        if not hit  == None:
            return hit[3].material.colorAt(hit[1], hit[2],vctr,self)
        return self.neutral
            
        
                    