from vector import * 

class Light():
    def __init__(self, direction = (1,1,1), color=(1,1,1)):
        self.direction = normalize(vec(direction[0],direction[1],direction[2]))
        self.color = vec(color[0],color[1],color[2])