from vector import *
from shape import * 

class AbstractRay():
    def pointAt(self, distance):
        """return the point at distance along the ray"""
        return self.point + distance*self.vector 
    def closestHit(self, world):
        """return (dist,point,normal,obj)"""
        return (None, None, None, None)

class Ray(AbstractRay):
    def __init__(self, point, vector, depth=0):
        self.point = point
        #print("Original vector " + str(vector))
        self.vector = normalize(vector)
        #print("Normalized vector " + str(self.vector))
        
#    def closestHit(self,world):
#        distance = 100000000000
#        minobj = None
#        for obj in world.objects:
#            temp = obj.hit(self)
#            #print("Temp = " + str(temp))
#            if not temp == None and temp[0] < distance:
#                minobj = obj
#                distance = temp[0]
#        if(not minobj == None):
#            return minobj.hit( self)
#        return None
#      
    def closestReflect(self, scene):
        dist, point, norm, obj = None, None, None, None  
        for o in scene.objects:
            hit = o.hit(self)
            if hit != None:
                if dist == None  and hit[0] > .001:
                    dist,point,norm,obj = hit
                elif hit[0] < dist:
                    dist,point,norm,obj = hit
        if dist == None:
            return None
        return (dist,point, norm, obj) 
       
    def closestHit(self, scene):
        dist, point, norm, obj = None, None, None, None  
        for o in scene.objects:
            hit = o.hit(self)
            if hit != None:
                if dist == None:
                    dist,point,norm,obj = hit
                elif hit[0] < dist:
                    dist,point,norm,obj = hit
        if dist == None:
            return None
        return (dist,point, norm, obj)        
                        