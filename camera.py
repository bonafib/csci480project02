import numpy 
from vector import *
from ray import *

class AbstractCamera():
    def ray(self, x, y):
        return Ray((0,0,0),(0,0,-1))


class Camera(AbstractCamera):
    def __init__(self,
                 eye = (0,0,10),
                 ul = (-10,10,-10),
                 ur = (10,10,-10),
                 ll = (-10,-10,-10),
                 lr = (10,-10,-10)):
        self.eye = vec(eye)
        self.ul = vec(ul)
        self.ur = vec(ur)
        self.ll = vec(ll)
        self.lr = vec(lr)

    def ray(self, x, y):
#        width = numpy.abs(self.ur[0]-self.ul[0])
#        #print("width = " + str(width))
#        height = numpy.abs(self.ur[1]-self.lr[1])
#        #print("height = " + str(height))
#        pos = numpy.array([width*x,height*y,self.ll[2]], dtype=float) + numpy.array([self.ll[0],self.ll[1],0])
##        print("x = " + str(x) + "y = " + str(y))
##        print(pos)
#        raye = pos - numpy.array(self.eye)
#        raye = vec(raye[0],raye[1],raye[2])
#        #pos = vec(self.eye[0],self.eye[1],self.eye[2])
#        return ray.Ray(point=pos, vector=raye)
        
        v1 = self.ul*(1-x) + self.ur*x
        v2 = self.ll*(1-x) + self.lr*x
        v = v1*(1-y) + v2*y
        return Ray(self.eye, normalize(v))
        
        