import numpy
import vector
import ray
import random
from material import *

EPSILON = 1.0e-10

class GeometricObject():
    def hit(self, ray):
        """Returns (t, point, normal, object) if hit
            and t > EPSILON"""
        """t is distance from self.point"""              
        
        return (None, None, None, None)
    def castShadows(self):
        return False

class Plane(GeometricObject):
    def __init__(self,point=(0,-10,-20), normal=(0,1,0),material=(Flat())):
        self.point = point
        self.normal = vector.normalize(normal)
        self.material = material
        
    def hit(self,ray):
        t = 0 
        if(numpy.dot(self.normal,ray.vector) != 0):
            t = numpy.dot(self.normal,(self.point-ray.point))/numpy.dot(self.normal,ray.vector) 
        if(t >= EPSILON):
            intersect = ray.point + t*ray.vector
            return(t,intersect,self.normal,self)
        return None
    
    def castShadows(self):
        return False

class Sphere(GeometricObject):
    def __init__(self, point=(0,0,0), radius=1,
                 material=None):
        if not(material):
            material = Phong()
        self.point = point
        self.radius = radius
        self.material = material
        
    def castShadows(self):
        return True


    def hit(self, ray):
        # assume sphere at origin, so translate ray:
        raypoint = ray.point - self.point
        a = numpy.dot(ray.vector, ray.vector)
        b = 2*numpy.dot(raypoint, ray.vector)
        c = numpy.dot(raypoint, raypoint) - self.radius*self.radius
        disc = b*b - 4*a*c
        if disc > 0.0:
            t = (-b-numpy.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)
            t = (-b+numpy.sqrt(disc))/(2*a)
            if t > EPSILON:
                p = ray.pointAt(t)
                n = normalize(p - self.point)
                return (t, p, n, self)        
        return None
 
def randpt(radius):
    return vec(random.random()*radius - radius, random.random()*radius - radius, random.random()*radius - radius)
    
def randomPlaneBlob(radius=4, center=vec((0,0,-20))
                    ,material = Reflector(), sides=24):
    planes = []
    for i in range(sides):
        planept = center + randpt(radius)
        planenorm = normalize(planept - center)
        planes.append(Plane(planenorm, planept, material))
    return PlaneBlob(planes)
                           
class PlaneBlob(GeometricObject):
    def __init__(self,planes):
        self.planes = planes
        self.shadow = True
        
    def castShadows(self):
        return self.shadow
    
    def hit(self, ray):
        front = None
        back = None
        for plane in self.planes:
            temp = plane.hit(ray)
            if(not temp == None):
                if( numpy.dot(plane.normal,ray.vector) < 0):
                    if(front == None or front[0] < temp[0]):
                        front = temp
                else:
                    if(back == None or back[0] > temp[0]):
                        back = temp
        if not front == None and not back == None and front[0] < back[0]:
            print(front[1])
            return front
        return None
                
                
                               
    